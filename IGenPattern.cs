﻿using System.Collections.Generic;

namespace _SmartCheckerWeb
{
    public interface IGenPattern : ISequencePattern
    {
        List<string> GenBasicAnswers(string test, string basicAnswer);
    }
}
