﻿using System.Collections.Generic;

namespace _SmartCheckerWeb
{
    public interface IPattern
    {
        string Name { get; }
        bool IsAnswerCorrect(List<string> studentAnswers, List<string> basicAnswers);
    }
}
