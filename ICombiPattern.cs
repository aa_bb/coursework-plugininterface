﻿using System.Collections.Generic;

namespace _SmartCheckerWeb
{
    public interface ICombiPattern : IPattern
    {
        List<IPattern> PatternList {get; set;}
        new string Name { get; set; }

        bool IsAnswerCorrect(List<string> studentAnswers, List<List<string>> basicAnswers);
    }
}
