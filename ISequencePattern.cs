﻿using System.Collections.Generic;

namespace _SmartCheckerWeb
{
    public interface ISequencePattern : IPattern
    {
        List<IPlainPattern> PatternList {get; set;}
    }
}
